import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor() { }

  students = [
    {
      "name": "Marko Ignjatovic",
      "email": "igmarko@gmail.com",
      "id": 0
    },
    {
      "name": "Sonja Meco",
      "email": "mezzosonja@gmail.com",
      "id": 1
    },
    {
      "name": "Dušan Škiljević",
      "email": "dusski@gmail.com",
      "id": 2
    },
    {
      "name": "Mateja Stojanović",
      "email": "mateyast@gmail.com",
      "id": 3
    },
    {
      "name": "Nemanja Rasic",
      "email": "nemanjarasic@hotmail.com",
      "id": 4
    },
    {
      "name": "Miljan Stojanovic",
      "email": "miljan93.nis@gmail.com",
      "id": 5
    },
    {
      "name": "Petar Begovic",
      "email": "petar.begovic1701@gmail.com",
      "id": 7
    },
    {
      "name": "Aleksandar Ivanovic",
      "email": "alleksa.iv@gmail.com",
      "id": 8
    },
    {
      "name": "Milan Stanisavljević",
      "email": "milan.stanisav@gmail.com",
      "id": 9
    },
    {
      "name": "Milan",
      "email": "milanpfc@hotmail.com",
      "id": 10
    },
    {
      "name": "Marko Miljković",
      "email": "markomiljkovic2@gmail.com",
      "id": 11
    }

  ]

  ngOnInit() {
  }

}
